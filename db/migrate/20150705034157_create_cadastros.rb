class CreateCadastros < ActiveRecord::Migration
  def change
    create_table :cadastros do |t|
      t.string :nome
      t.string :email
      t.string :telefone
      t.string :celular

      t.timestamps null: false
    end
  end
end
