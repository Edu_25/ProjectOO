class CreateAgendamentos < ActiveRecord::Migration
  def change
    create_table :agendamentos do |t|
      t.string :Nome
      t.text :Descricao
      t.date :date

      t.timestamps null: false
    end
  end
end
