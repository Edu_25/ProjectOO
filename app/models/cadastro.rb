class Cadastro < ActiveRecord::Base
	validates :nome, presence: true, length: { maximum: 50 } 
	before_save { self.email = email.downcase } 
	validates :email, presence: true, format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, on: :create}, uniqueness: {case_sensitive: false} 
	def self.search(palavra)
		where("nome like ?", "%#{palavra}%")
	end
end
